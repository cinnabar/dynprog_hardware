module cascaded_iterator_tb();
    `SETUP_TEST
    reg clk;
    reg rst;

    reg count_enable;
    wire result_valid;
    wire output_count_enable;
    wire[4:0] result;

    initial begin
        $dumpfile(`VCD_OUTPUT);
        $dumpvars(0, cascaded_iterator_tb);
        clk = 0;
        #1;
        forever begin
            #1 clk = ~clk;
        end
    end

    initial begin
        rst <= 1;
        @(negedge clk)
        rst <= 0;
        count_enable <= 1;
        @(negedge clk)
        `ASSERT_EQ(result_valid, 1)
        `ASSERT_EQ(result, 0)
        #20
        `END_TEST
    end

    SingleValueIterator svi
        ( .clk(clk)
        , .rst(rst)
        , .count_enable({'1, count_enable})
        , .result({result_valid, result, output_count_enable})
        );
endmodule
