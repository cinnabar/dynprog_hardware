module pipeline_tb();
    `SETUP_TEST
    reg clk;
    reg rst;

    wire[`COST_SIZE-1:0] cost;
    wire[`LAYER_STATE_SIZE-1:0] cost_read_state;
    reg input_valid;
    wire write_enable;

    initial begin
        $dumpfile(`VCD_OUTPUT);
        $dumpvars(0, pipeline_tb);
        clk = 0;
        input_valid = 0;
        #1;
        forever begin
            #1 clk = ~clk;
        end
    end

    localparam RAM_MODE_CONSTANT_VALUE = 0;
    localparam RAM_MODE_SINGLE_POINT = 1;
    reg ram_mode;

    reg[`COST_SIZE-1:0] ram_constant_value;
    reg[`COST_SIZE-1:0] cost_from_ram;
    reg[`LAYER_STATE_SIZE-1:0] ram_cheap_point = 0;

    reg[`LAYER_STATE_SIZE-1:0] starting_state;
    initial begin
        starting_state = 0;
        ram_mode <= RAM_MODE_CONSTANT_VALUE;
        ram_constant_value <= 1 << 12;
        rst <= 1;
        @(negedge clk)
        rst <= 0;
        // Wait for the first input to propagate
        @(posedge pl.min_cost_valid)
        @(negedge clk)
        `ASSERT_EQ(cost, (7 + 7 + 1 + 1) << 12); // Cost of inputs plus cost of previous stage plus transition
        @(negedge clk)
        `ASSERT_EQ(cost, (6 + 7 + 1 + 1) << 12); // Cost of inputs plus cost of previous stage
        @(posedge write_enable);
        @(negedge clk)
        `ASSERT_EQ(cost, (1 + 1) << 12)

        // Any state reachable from an infeasible state should be infeasible
        ram_constant_value <= 'hfffff;
        rst <= 1;
        @(posedge clk)
        rst <= 0;
        @(posedge write_enable);
        @(negedge clk)
        `ASSERT_EQ(cost, 'hfffff)
        rst <= 1;
        @(posedge clk)
        rst <= 0;
        // Ensure that the lowest cost is correctly calculated when the cost
        // from ram goes from inf to 0
        ram_constant_value <= 'hfffff;
        #100
        ram_constant_value <= 'h0;
        // Wait for pipeline delays
        #9
        `ASSERT_NE(cost, 16'hffff);

        // Ensure that traveling from 1,1 to 0,0 costs 3 if 0,0 is the only
        // cheap node
        rst <= 1;
        ram_mode = RAM_MODE_SINGLE_POINT;
        ram_cheap_point = 'h00_00;
        starting_state = 'h11;
        #2
        rst <= 0;
        @(posedge write_enable)
        @(negedge clk)
        `ASSERT_EQ(cost, 4 << 12);
        #10
        `END_TEST
    end

    always @(posedge clk) begin
        if (ram_mode == RAM_MODE_CONSTANT_VALUE)
            cost_from_ram <= ram_constant_value;
        else
            if (cost_read_state == ram_cheap_point)
                cost_from_ram = 'h1 << 12;
            else
                cost_from_ram = 'hfffff;
    end

    Pipeline pl (
        .clk(clk),
        .rst(rst)
        // Input specifying that the current input is valid
        , .input_valid('1)
        // The state to search from
        , .starting_state(starting_state)
        // Output indicating that the calculation is done and the result should be
        // written to ram
        , .write_enable(write_enable)
        // The resulting minimum cost from this starting state. Only valid if
        // write_enable is 1
        , .cost_to_write(cost)
        // Address to read previous cost to go from
        , .cost_read_state(cost_read_state)
        // The cost that was read from ram at cost_read_state
        , .cost_from_ram(cost_from_ram)
    );
endmodule

