module main_tb();
    `SETUP_TEST
    reg clk;
    reg rst;

    reg[31:0] add_1;
    reg[31:0] add_2;
    wire[31:0] add_out;
    wire[31:0] parallel_test_out;
    reg input_valid;

    initial begin
        $dumpfile(`VCD_OUTPUT);
        $dumpvars(0, main_tb);
        clk = 0;
        input_valid = 0;
        #1;
        forever begin
            #1 clk = ~clk;
        end
    end

    initial begin
        rst = 1;
        @(negedge clk)
        rst = 0;
        add_1 = 10;
        add_2 = 5;
        input_valid = 1;
        @(negedge clk)
        add_1 = 100;
        add_2 = 50;
        @(negedge clk)
        add_1 = 500;
        add_2 = 1000;
        #20
        `END_TEST
    end

    adder adder_test(
        .clk(clk),
        .rst(rst),
        .a(add_1),
        .b(add_2),
        .sum(add_out),
        .input_valid(input_valid)
    );
    parallel_test pt(
        .clk(clk),
        .rst(rst),
        .a(add_1),
        .b(add_2),
        .result(parallel_test_out),
        .input_valid(input_valid)
    );
endmodule
