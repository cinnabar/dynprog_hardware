module pipeline_after_model_tb();
    `SETUP_TEST
    reg clk;
    reg rst;

    wire[`COST_SIZE-1:0] cost;
    wire[`STATE_SIZE-1:0] cost_read_address;
    reg input_valid;
    wire write_enable;

    initial begin
        $dumpfile(`VCD_OUTPUT);
        $dumpvars(0, pipeline_after_model_tb);
        clk = 0;
        input_valid = 0;
        #1;
        forever begin
            #1 clk = ~clk;
        end
    end

    reg[`COST_SIZE-1:0] from_ram;
    reg local_cost_valid;
    reg[31:0] local_cost;

    wire result_valid;
    wire[`COST_SIZE-1:0] result;

    reg payload;

    initial begin
        payload = 0;
        rst <= 1;
        local_cost_valid <= 0;
        #2
        rst <= 0;
        local_cost <= 5;
        local_cost_valid <= 1;
        from_ram <= 3;
        @(posedge result_valid)
        @(negedge clk)
        `ASSERT_EQ(result, 8)
        #2
        rst <= 1;
        #2
        `ASSERT_EQ(result_valid, 0)
        from_ram <= 'hfffff;
        rst <= 0;
        @(posedge result_valid)
        @(negedge clk)
        `ASSERT_EQ(result, 'hfffff)
        payload = 1;
        @(negedge clk)
        payload = 0;
        @(negedge clk)
        @(negedge clk)
        @(negedge clk)
        `ASSERT_EQ(payload_delayed, 1)
        @(negedge clk)
        `ASSERT_EQ(payload_delayed, 0)
        #10
        `END_TEST
    end

    wire payload_delayed;


    PipelineAfterModel uut
        ( .clk(clk)
        , .rst(rst)
        , .from_ram(from_ram)
        , .payload(payload)
        , .local_cost_valid(local_cost_valid)
        , .local_cost(local_cost)
        , .output_valid(result_valid)
        , .min_cost(result)
        , .payload_delayed(payload_delayed)
        );
endmodule

