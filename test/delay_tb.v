module delay_tb();
    `SETUP_TEST
    reg clk;
    reg rst;

    reg[1:0] to_delay;
    wire[1:0] out;
    wire valid;

    initial begin
        $dumpfile(`VCD_OUTPUT);
        $dumpvars(0, delay_tb);
        clk = 0;
        #1;
        forever begin
            #1 clk = ~clk;
        end
    end

    initial begin
        rst <= 1;
        @(negedge clk)
        rst <= 0;
        to_delay <= 'b01;
        `ASSERT_EQ(to_delay_delayed_valid, 0)
        #2
        `ASSERT_EQ(to_delay_delayed_valid, 0)
        to_delay <= 'b10;
        #2
        `ASSERT_EQ(to_delay_delayed_valid, 1)
        `ASSERT_EQ(to_delay_delayed, 'b01)
        #2
        `ASSERT_EQ(to_delay_delayed, 'b10)
        #10;
        `END_TEST
    end

    `DELAY(clk, rst, to_delay, to_delay_delayed, 2 ,2)
endmodule

