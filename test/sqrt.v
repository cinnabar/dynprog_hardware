module sqrt_tb();
    `SETUP_TEST
    reg clk;
    reg rst;

    reg[31:0] in;
    wire[31:0] out;


    initial begin
        $dumpfile(`VCD_OUTPUT);
        $dumpvars(0, sqrt_tb);
        clk = 0;
        #1;
        forever begin
            #1 clk = ~clk;
        end
    end
    initial begin
        // Sqrt(1) == 1
        in <= 1 << 12;
        @(negedge clk)
        `ASSERT_EQ(out, 1 << 12);

        // Sqrt(4) == 2
        in <= 4 << 12;
        @(negedge clk)
        `ASSERT_EQ(out, 2 << 12);

        // Sqrt(2) ~= 1.4142
        // round(sqrt(x) * 4096)
        in <= 2 << 12;
        @(negedge clk)
        `ASSERT_EQ(out, 5792);

        // Sqrt(289) = 17
        // round(sqrt(x) * 4096)
        in <= 289 << 12;
        @(negedge clk)
        `ASSERT_EQ(out, 69632);

        // Sqrt(0.5) = 0.707
        in <= 1 << 11;
        @(negedge clk)
        `ASSERT_EQ(out, 2896);

        // Sqrt(0.3) = 0.5477
        in <= 1228;
        @(negedge clk)
        `ASSERT_EQ(out, 2243);



        #10
        `END_TEST
    end

    sqrt uut(.clk(clk), .x(in), .y(out));
endmodule

