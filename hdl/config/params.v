// The amount of states, excluding time
`define STATE_AMOUNT 2
`define STATE_SIZE_0 4
`define STATE_SIZE_1 4
`define TIME_SIZE 10
`define LAYER_STATE_SIZE (`STATE_SIZE_0 + `STATE_SIZE_1)
`define STATE_SIZE (`LAYER_STATE_SIZE + `TIME_SIZE)

`define COST_SIZE 24

`define INPUT_AXIS_AMOUNT 3
`define INPUT_LUT_SIZE_1 4
`define INPUT_LUT_SIZE_2 4
`define INPUT_LUT_SIZE_3 4
`define INPUT_SIZE_1 16
`define INPUT_SIZE_2 16
`define INPUT_SIZE_3 16
