`include "hdl/delay.v"

module Pipeline
    ( clk
    , rst
    // Input specifying that the current input is valid
    , input_valid
    // The state to search from
    , starting_state
    // Output indicating that the calculation is done and the result should be
    // written to ram
    , write_enable
    // The resulting minimum cost from this starting state. Only valid if
    // write_enable is 1
    , cost_to_write
    // The best inputs in this starting state. Only valid if write_enable is 1
    , input1_to_write
    , input2_to_write
    , input3_to_write
    // Address to read previous cost to go from
    , cost_read_state
    // The cost that was read from ram at cost_read_address
    , cost_from_ram
    );

    input clk;
    input rst;
    input input_valid;
    input[`LAYER_STATE_SIZE -1:0] starting_state;
    input[`COST_SIZE - 1:0] cost_from_ram;
    output reg write_enable;
    output reg[`COST_SIZE -1:0] cost_to_write;
    output reg[`INPUT_SIZE_1] input1_to_write;
    output reg[`INPUT_SIZE_2] input2_to_write;
    output reg[`INPUT_SIZE_3] input3_to_write;
    output wire[`LAYER_STATE_SIZE -1:0] cost_read_state;

    wire[`INPUT_SIZE_1 - 1:0] input_axis0;
    wire[`INPUT_SIZE_2 - 1:0] input_axis1;
    wire[`INPUT_SIZE_3 - 1:0] input_axis2;

    wire[31:0] model_output;
    reg[31:0] lowest_cost;
    // Output of the model
    reg[17:0] local_cost;

    wire[17:0] out_state_0;
    wire[16:0] out_state_1;

    wire[`STATE_SIZE_0 - 1:0] starting_state_0;
    wire[`STATE_SIZE_1 - 1:0] starting_state_1;
    assign starting_state_0 = starting_state[`STATE_SIZE_0 + `STATE_SIZE_1 - 1:`STATE_SIZE_0];
    assign starting_state_1 = starting_state[`STATE_SIZE_0 - 1:0];

    // Delays the input_valid signals for 2 clock cycles into s2_input_valid
    `DELAY_STAGE(input_valid, 2, 1)

    wire done_delayed;

    before_pipeline before_pl(
        .clk(clk),
        .rst(rst),
        .input_valid(input_valid),
        .input_axis0(input_axis0),
        .input_axis1(input_axis1),
        .input_axis2(input_axis2),
        .done_delayed(done_delayed)
    );

    wire[15:0] delayed_input_0;
    wire[15:0] delayed_input_1;

    // TODO: Uniform comma style
    // Pass parameters to model
    wire model_output_ready;
    sample_model model (
        .clk(clk),
        .rst(rst),
        .input_valid(s2_input_valid),
        .output_valid(model_output_ready),
        // Sign extended inputs
        .input_ax0(input_axis0),
        .input_ax1(input_axis1),
        .input_ax2(input_axis2),
        .state_0({starting_state_0, 12'h0}),
        .state_1({starting_state_1, 12'h0}),
        .delayed_input_0(delayed_input_0),
        .delayed_input_1(delayed_input_1),
        .out_state_0(out_state_0),
        .out_state_1(out_state_1),
        .cost(local_cost)
    );

    reg model_output_valid;
    always @(*) begin
        // Check if the model output is within bounds and ready to be read.
        if((out_state_0[17:`STATE_SIZE_0+12] == 0) && (out_state_1[16:`STATE_SIZE_1+12] == 0) && model_output_ready)
            model_output_valid = 1;
        else
            model_output_valid = 0;
    end

    wire min_cost_valid;
    wire[`COST_SIZE-1:0] min_cost;
    wire state_done;


    PipelineAfterModel after_model
        ( .clk(clk)
        , .rst(rst)
        // Inputs
        , .from_ram(cost_from_ram)
        , .local_cost_valid(model_output_valid)
        , .local_cost({14'b0, local_cost})
        // TODO Pass delayed inptus
        , .inputs(96'b0)
        // Outputs
        , .payload(done_delayed)
        , .output_valid(min_cost_valid)
        , .min_cost(min_cost)
        // TODO: Read delayed inputs
        , .payload_delayed(state_done)
        );

    assign cost_to_write = min_cost;
    assign write_enable = state_done;
    // TODO: Replace with proper fixpoint location
    assign cost_read_state = {out_state_1[`STATE_SIZE_0 + 11:12], out_state_0[`STATE_SIZE_1 + 11:12]};
endmodule
