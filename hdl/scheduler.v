module Scheduler
        ( clk
        , rst
        , goal_state_0
        , goal_state_1
        );
    input clk;
    input rst;
    input[`STATE_SIZE_0 - 1:0] goal_state_0;
    input[`STATE_SIZE_1 - 1:0] goal_state_1;


    reg[`TIME_SIZE-1:0] current_time = 1;
    wire input_valid;
    wire[`LAYER_STATE_SIZE - 1:0] starting_state;
    wire pl_write_enable;
    wire[`COST_SIZE-1:0] cost_to_write;

    wire[`STATE_SIZE_0-1:0] state_axis_0;
    wire[`STATE_SIZE_1-1:0] state_axis_1;
    wire[3:0] state_axis_2;

    wire cascading_done;
    reg count_enable;

    // TODO: Fix this ugly done calculation
    CascadedIterator ci
        ( .clk(clk)
        , .rst(rst)
        , .count_enable(count_enable)
        , .ax0(state_axis_0)
        , .ax1(state_axis_1)
        , .ax2(state_axis_2)
        // , .done(cascading_done)
        );
    assign cascading_done = state_axis_2[0];

    assign starting_state = {state_axis_0, state_axis_1};

    wire[`STATE_SIZE-1:0] read_addr;
    wire[`COST_SIZE-1:0] cost_ram_output;

    // True once the first "layer" in RAM has been initialised to inf and the
    // goal node set to 0
    reg setup_done;
    always @(posedge clk) begin
        if(rst)
            setup_done <= 0;
        else
            if (cascading_done == 1)
                setup_done <= 1;
    end

    assign input_valid = setup_done;

    // Initialise RAM 
    reg[`STATE_SIZE-1:0] ram_input_addr;
    reg ram_write_enable;
    reg[`COST_SIZE-1:0] ram_input;
    always @(
        setup_done,
        state_axis_0,
        state_axis_1,
        pl_write_enable,
        starting_state,
        current_time
    ) begin
        if (setup_done) begin
            ram_input_addr <= {current_time, starting_state};
            ram_write_enable <= pl_write_enable;
            ram_input <= cost_to_write[`COST_SIZE-1:0];
            count_enable <= pl_write_enable;
        end
        else begin
            ram_input_addr <= {10'b0, starting_state};
            ram_write_enable <= 1;
            count_enable <= 1;
            if (state_axis_0 == goal_state_0 && state_axis_1 == goal_state_1)
                ram_input <= 'h0;
            else
                ram_input <= 'hffff;
        end
    end

    wire[`LAYER_STATE_SIZE - 1: 0] cost_read_state;

    Pipeline pl
        ( .clk(clk)
        , .rst(rst | !setup_done | pl_write_enable)
        , .input_valid(input_valid)
        , .starting_state(starting_state)
        , .write_enable(pl_write_enable)
        , .cost_to_write(cost_to_write)
        , .cost_read_state(cost_read_state)
        , .cost_from_ram(cost_ram_output)
        );

    wire[`TIME_SIZE-1: 0] read_time = current_time - 1;
    assign read_addr = {read_time, cost_read_state};

    sram ram
        ( .clk(clk)
        , .i_addr(ram_input_addr)
        , .i_data(ram_input)
        , .write(ram_write_enable)
        , .o_addr(read_addr)
        , .o_data(cost_ram_output)
        );
endmodule
