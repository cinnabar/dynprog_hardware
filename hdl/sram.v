// https://timetoexplore.net/blog/block-ram-in-verilog-with-vivado

module sram #(parameter ADDR_WIDTH = `STATE_SIZE, DATA_WIDTH = `COST_SIZE, DEPTH = `STATE_SIZE) (
    input wire clk,
    input wire [ADDR_WIDTH-1:0] o_addr,
    input wire [ADDR_WIDTH-1:0] i_addr,
    input wire write,
    input wire [DATA_WIDTH-1:0] i_data,
    output reg [DATA_WIDTH-1:0] o_data
    );

    reg [DATA_WIDTH-1:0] memory_array [0:2**DEPTH-1];

    always @ (posedge clk)
    begin
        if(write) begin
            memory_array[i_addr] <= i_data;
        end
        o_data <= memory_array[o_addr];
    end
endmodule
