`ifndef V_DELAY
`define V_DELAY

// Delay the specified signal for `DELAY` clock cycles. The valid bit
// indicates wether the first value after the last reset has propagated.
module Delay #(parameter DELAY = 2, parameter VAR_SIZE = 1)
    ( clk
    , rst
    , in
    , out
    , valid
    );
    input clk;
    input rst;
    input[VAR_SIZE-1:0] in;
    output[VAR_SIZE-1:0] out;
    output valid;

    reg[DELAY*VAR_SIZE - 1: 0] out_buffer;
    reg[DELAY-1:0] valid_buffer;
    always @(posedge clk) begin
        if (rst) begin
            valid_buffer <= 0;
        end
        else begin
            valid_buffer <= {valid_buffer[DELAY-2:0], '1};
            out_buffer <= {out_buffer[(DELAY-1) * VAR_SIZE - 1:0], in};
        end
    end

    assign valid = valid_buffer[DELAY-1];
    assign out = out_buffer[DELAY * VAR_SIZE - 1:(DELAY-1) * VAR_SIZE];
endmodule


`define DELAY1(clk_, rst_, name, delayed_name, size) reg [size-1:0] delayed_name; \
    always @(posedge clk_) begin \
        if (rst_) \
            delayed_name <= 0; \
        else \
            delayed_name <= name;\
    end


`define DELAY(clk_, rst_, name, delayed_name, cycles, size) wire[size-1:0] delayed_name; \
    wire ``delayed_name``_valid; \
    Delay #(.DELAY(cycles), .VAR_SIZE(size)) ``name``_delay_module \
    ( .clk(clk_) \
    , .rst(rst_) \
    , .in(name) \
    , .out(delayed_name) \
    , .valid(``delayed_name``_valid) \
    );

`define DELAY_STAGE(name, to_stage, size) \
    `DELAY1(clk, rst, name, s``to_stage``_``name``, size)

`endif
