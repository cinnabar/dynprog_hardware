import math

for x, y in [(x, round(2**((x/4096.)) * 4096.)) for x in range(0, 4096)]:
    print(f"'h{x:x}: y <= 'h{y:x};")
