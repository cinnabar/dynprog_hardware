#!/usr/bin/python3

import random

x1inputs = [
    0,
    1,
    4,
    9,
    16,
    25,
    36,
    49,
    64,
    81,
    100,
    121,
    144,
    169,
    196,
    225,
    256,
    289,
    324,
    361,
    400,
    441,
    484,
    529,
    576,
    625,
    676,
    729,
    784,
    841,
    900,
    961,
    1024,
    1089,
    1156
]
x2inputs = [
    0.3000,
    0.3125,
    0.3250,
    0.3375,
    0.3500,
    0.3625,
    0.3750,
    0.3875,
    0.4000,
    0.4125,
    0.4250,
    0.4375,
    0.4500,
    0.4625,
    0.4750,
    0.4875,
    0.5000,
    0.5125,
    0.5250,
    0.5375,
    0.5500,
    0.5625,
    0.5750,
    0.5875,
    0.6000,
    0.6125,
    0.6250,
    0.6375,
    0.6500,
    0.6625,
    0.6750,
    0.6875,
    0.7000,
    0.7125,
    0.7250,
    0.7375,
    0.7500,
    0.7625,
    0.7750,
    0.7875,
    0.8000,
]

u1inputs = [
    -360,
    -336,
    -312,
    -288,
    -264,
    -240,
    -216,
    -192,
    -168,
    -144,
    -120,
    -96,
    -72,
    -48,
    -24,
    0,
    24,
    48,
    72,
    96,
    120,
    144,
    168,
    192,
    216,
    240,
    264,
    288,
    312,
    336,
    360,
]
u2inputs = [
    -42.0000,
    -37.8000,
    -33.6000,
    -29.4000,
    -25.2000,
    -21.0000,
    -16.8000,
    -12.6000,
    -8.4000,
    -4.2000,
    0,
    4.2000,
    8.4000,
    12.6000,
    16.8000,
    21.0000,
    25.2000,
    29.4000,
    33.6000,
    37.8000,
    42.0000,
]
u3inputs = [1,2,3,4,5,6]

def fill_template(file, replacement):
    result = ""
    with open(file) as f:
        lines = f.readlines()
    for line in lines:
        if line.strip() == "{{code}}":
            result += replacement
        else:
            result += line
    return result


def var_dump_verilog():
    result = "";
    with open("../hdlconverter/models/vars.txt") as f:
        lines = f.readlines();
        for line in lines:
            result += f"    $fwrite(f, \"{line.strip()}: %f\\n\", $signed(uut.{line.strip()}) / 4096.0);\n"
    return result

def generate_verilog(i, x1, x2, u1, u2, u3):
    return f"""
    x1 = {x1 * 4096};
    x2 = {x2 * 4096};
    u1 = {u1 * 4096};
    u2 = {u2 * 4096};
    u3 = {u3 * 4096};
    dt = {10 * 4096};
    rst<=1;
    @(posedge clk)
    rst<=0;
    @(posedge valid);

    f = $fopen("output/vardump_{i}.txt", "w");
    {var_dump_verilog()}

    $write("{i}: ");
    $write(failed);
    $write(", ");
    $write("%f", $signed(stage_cost) / 4096.0);
    $write(",\\t");
    $write("%f", $signed(x1next) / 4096.0);
    $write(",\\t");
    $write("%f", $signed(x2next) / 4096.0);
    $write(",\\t");
    $write("%f", $signed(fuel) / 4096.0);
    $write(",\\t");
    $write("%f", $signed(time_) / 4096.0);
    $write(",\\t");
    $write("%f", $signed(teng) / 4096.0);
    $write(",\\t");
    $write("%f", $signed(fbrk) / 4096.0);
    $write("\\n");
    """

def generate_cpp(i, x1, x2, u1, u2, u3):
    return f"""
    failed = reference_pt_model({x1}, {x2}, {u1}, {u2}, {u3}, 10, stage_cost, x1next, x2next, fuel, time, teng, fbrk) != ReferenceResult::OK;
    stream << "{i}: " << failed << ", " << *stage_cost << ", " << *x1next << ", " << *x2next << ", " << *fuel
        << ", " << *time << ", " << *teng << ", " << *fbrk << std::endl;
    """


def main():
    random.seed(1)
    verilog = ""
    cpp = ""
    for i in range(10):
        x1 = random.sample(x1inputs, 1)[0]
        x2 = random.sample(x2inputs, 1)[0]
        u1 = random.sample(u1inputs, 1)[0]
        u2 = random.sample(u2inputs, 1)[0]
        u3 = random.sample(u3inputs, 1)[0]

        verilog += generate_verilog(i, x1, x2, u1, u2, u3)
        cpp += generate_cpp(i, x1, x2, u1, u2, u3)

    verilog_file = open("test/samples.v", "w")
    verilog_file.write(fill_template(
        "templates/test_verilog.v",
        verilog
    ))
    cpp_file = open("../hdlconverter/samples.cpp", "w")
    cpp_file.write(fill_template(
        "templates/test_cpp.cpp",
        cpp
    ))


if __name__ == "__main__":
    main()
