#!/bin/python

import sys
import math

def build_case_branch(output_name, key, value):
    return "{}: {} <= {};".format(key, output_name, value)


def build_case_stmt(output_name, condition_name, values):
    result = []
    result.append("case({})".format(condition_name))
    for i, val in enumerate(values):
        result.append(build_case_branch(output_name, i, val))
    result.append(build_case_branch(output_name, "default", 0))
    result.append("endcase")
    return result


def build_lut_module(module_name, values, input_bits, output_bits):
    result = []
    # Variable names
    input_name = "addr"
    output_name = "result"
    buffer_name = "buffer"

    # Module header and extra registers
    result.append(f"module {module_name}(input clk, input[{input_bits-1}:0] {input_name}, output[{output_bits-1}:0] {output_name});")
    result.append(f"reg[{output_bits-1}:0] {buffer_name};")
    result.append(f"assign {output_name} = {buffer_name};")

    # Case statement in always block
    result.append(f"always @(posedge clk) begin")
    result += build_case_stmt(buffer_name, input_name, values)
    result.append("end")

    result.append("endmodule")
    return result


def lut_from_input_file(filename, lut_name):
    with open(filename) as f:
        values = list(map(lambda s: int(s.strip()) * 2**12, f.readlines()))
        input_bits = math.ceil(math.log2(len(values)))

        # +1 because we need the sign bit
        output_bits = math.ceil(math.log2(max(map(abs, values))) + 1)
        return build_lut_module(lut_name, values, input_bits, output_bits)


def main():
    if len(sys.argv) == 3:
        print("\n".join(lut_from_input_file(sys.argv[1], sys.argv[2])))
    else:
        print("Usage: build_lut <filename> <module_name>")
        exit(-1)


if __name__ == "__main__":
    main()
