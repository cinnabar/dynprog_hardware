import math

for x, y in [(x, round(math.log2(1+(x/4096.)) * 4096.)) for x in range(1, 4096)]:
    print(f"'h{x:x}: y <= 'h{y:x};")
