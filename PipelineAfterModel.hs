module PipelineAfterModel where

import Clash.Prelude
import Clash.Explicit.Testbench

-- For some reason clash does not define this function. Returns true if a maybe
-- has a value
isJust :: Maybe a -> Bool
isJust m =
    case m of
        Just _ -> True
        Nothing -> False


type Payload = Bit
type Cost = Unsigned 24
type UnsaturatedCost = Unsigned 32
type Inputs = (Unsigned 32, Unsigned 32, Unsigned 32)
data DataBundle = DataBundle {cost :: UnsaturatedCost, inputs :: Inputs}
data OutputBundle = OutputBundle {outputCost :: Cost, outputInputs :: Inputs}

-- Add extra cost to a data bundle
addCost :: UnsaturatedCost -> DataBundle -> DataBundle
addCost newCost bundle =
    bundle {cost = cost bundle + newCost}


-- Delay the input for 1 clock cycle
ramWaitStage ::
    (HiddenClockReset domain gated synchronous)
    => DSignal domain n (Maybe DataBundle)
    -> DSignal domain (n+1) (Maybe DataBundle)
ramWaitStage input =
    delayedI input


-- Add the cost from RAM to the local cost from the model which is supplied by
-- the previous stage
costSumStage
    :: (KnownNat n, HiddenClockReset domain gated synchronous)
    => Signal domain UnsaturatedCost
    -> DSignal domain n (Maybe DataBundle)
    -> DSignal domain (n + 1) (Maybe DataBundle)
costSumStage fromRam fromPrevStage =
    let
        -- Convert the signal passed from RAM into a delayed signal. Since we are
        -- in the middle of the pipeline,, unsafeFromSignal has to be used.
        delayedFromRam = unsafeFromSignal fromRam

        -- Performs the actual addition
        innerFunction :: UnsaturatedCost -> Maybe DataBundle -> Maybe DataBundle
        innerFunction fromRam fromPrevStage =
            fmap (addCost fromRam) fromPrevStage
    in
        -- Perform the addition and delay the output by 1 cycle
        delayedI $ liftA2 innerFunction delayedFromRam fromPrevStage


-- Select the smallest between an accumulated value and a new value
minAccumulate :: (KnownNat n, HiddenClockReset domain gated synchronous)
    => DSignal domain n (Maybe DataBundle) -> DSignal domain n (Maybe DataBundle)
minAccumulate new = feedback (min' new)
  where
    -- Inner function passed to feedback function that performs the min accumulate operation
    min' ::(KnownNat n, HiddenClockReset domain gated synchronous)
         => DSignal domain n (Maybe DataBundle)
         -> DSignal domain n (Maybe DataBundle)
         -> (DSignal domain n (Maybe DataBundle), DSignal domain (n+1) (Maybe DataBundle))
    min' new acc =
        let
            -- Function to apply to the inputs. Ensures that the smallest valid
            -- value observed so far is selected
            minFn min acc =
                case (min, acc) of
                    (Just x, Just y) ->
                        if cost x < cost y then Just x else Just y
                    (Nothing, Just y) ->
                        Just y
                    (Just x, Nothing) -> Just x
                    (Nothing, Nothing) ->
                        Nothing

            -- Apply minFn function to the delayed signals
            acc' = liftA2 minFn new acc
        in
            -- Return the result. The default value for the accumulator
            -- register is set to Nothing
            (acc, delayed (singleton (Nothing)) acc')


-- Stage for selecting the minimum of the observed values
minStage
    :: (KnownNat n, HiddenClockReset domain gated synchronous)
    => DSignal domain n (Maybe DataBundle)
    -> DSignal domain (n + 1) (Maybe DataBundle)
minStage input =
    delayedI $ minAccumulate input



-- Unsigned saturation.
saturate :: DataBundle -> OutputBundle
saturate original =
    let
        saturated = if cost original > 0xfffff then 0xfffff else truncateB $ cost original
    in
        OutputBundle saturated (inputs original)

-- Stage for performing saturation
saturationStage
    :: (KnownNat n, HiddenClockReset domain gated synchronous)
    => DSignal domain n (Maybe DataBundle)
    -> DSignal domain (n + 1) (Maybe OutputBundle)
saturationStage input =
    delayedI $ fmap (fmap saturate) input



-- The main pipeline that performs actual computations. Built from a sequence
-- of each stage. The length could probably be infered but must be specified to
-- make the compiler happy
mainPipeline :: (HiddenClockReset domain gated synchronous)
         => Signal domain UnsaturatedCost
         -> DSignal domain 0 (Maybe DataBundle)
         -> DSignal domain 4 (Maybe OutputBundle)
mainPipeline fromRam =
    saturationStage . minStage . costSumStage fromRam . ramWaitStage


type ModuleInput a = (Cost, Bit, UnsaturatedCost, Inputs, a)
type ModuleOutput a = (Bit, Cost, Inputs, a)

-- Main module which receives signals from RAM, from the model and additional payload
-- to be delayed together with the pipeline.
stageModule :: (Num a, HiddenClockReset domain gated synchronous) =>
    Signal domain (ModuleInput a) -> Signal domain (ModuleOutput a)
stageModule inputRaw =
    let
        parseInput :: ModuleInput a -> (Cost, Maybe DataBundle, a)
        parseInput (fromRam, costValid, costFromModel, inputs, payload) =
            ( fromRam
            , if costValid==1 then Just $ DataBundle costFromModel inputs else Nothing
            , payload
            )

        -- Split the input into separate Signals
        (fromRam, pipelineInput, payload) = unbundle $ fmap parseInput inputRaw

        -- The cost received from ram is only 16 bits, expand it to 32
        fromRamExt = fmap zeroExtend fromRam

        -- Convert the payload into a delayed signal
        payloadDelayed = fromSignal payload

        -- Converts the Maybe <result> into individual signals
        unbundleOutput :: (Maybe OutputBundle, a) -> ModuleOutput a
        unbundleOutput (mainData, payload) =
            let
                valid = isJust $ mainData
                cost = maybe 0 (outputCost) mainData
                inputs = maybe (0,0,0) outputInputs mainData
            in
                (unpack (pack valid), cost, inputs, payload)
    in
        fmap unbundleOutput $
            -- Convert the result back to a non-delayed signal
            toSignal
                -- Merge the delay pipeline with the computation pipeline
                $ liftA2
                    (,)
                    -- Main pipeline
                    (mainPipeline fromRamExt $ fromSignal pipelineInput)
                    -- Delayed payload
                    (delayed (repeat 0) payloadDelayed)



{-# ANN topEntity
  (Synthesize
    { t_name   = "PipelineAfterModel"
    , t_inputs = [ PortName "clk"
                 , PortName "rst"
                 , PortProduct ""
                    [ PortName "from_ram"
                    , PortName "local_cost_valid"
                    , PortName "local_cost"
                    , PortName "inputs"
                    , PortName "payload"
                    ]
                 ]
    , t_output = PortProduct ""
        [ PortName "output_valid"
        , PortName "min_cost"
        , PortName "best_inputs"
        , PortName "payload_delayed"
        ]
    }) #-}
topEntity
  :: Clock System Source
  -> Reset System Synchronous
  -> Signal System (ModuleInput Payload)
  -> Signal System (ModuleOutput Payload)
topEntity = exposeClockReset stageModule
