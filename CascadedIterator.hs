module CascadedIterator where

import Clash.Prelude
import Clash.Explicit.Testbench

data CounterStatus = Counting | Max | Wraparound

singleValueIterator ::
    (KnownNat n, Num a, Eq a, HiddenClockReset domain gated synchronous)
    => a
    -> DSignal domain n (Maybe (rest, Bool))
    -> DSignal domain n (Maybe (a, Bool))
singleValueIterator maxValue fromPrevious =
    delayedI $ feedback $ iterator' maxValue $ countEnable
    where
        countEnable = fmap (fmap (\(rest, countEnable) -> (0, countEnable))) fromPrevious
        iterator' ::
             (Num a, Eq a, KnownNat n, HiddenClockReset domain gated synchronous)
             => a
             -> DSignal domain n (Maybe (a, Bool))
             -> DSignal domain n (Maybe (a, Bool))
             -> (DSignal domain n (Maybe (a, Bool)), DSignal domain (n+1) (Maybe (a, Bool)))
        iterator' maxValue countEnable acc =
            let
                countFn acc countEnable =
                    case (acc, countEnable) of
                        (Just prev, Just True) ->
                            if prev == maxValue then Just (0, True) else Just (prev + 1, False)
                        (Just prev, Just False) ->
                            Just (prev, False)
                        (Nothing, Just _) ->
                            Just (0, False)
                        (Nothing, Nothing) ->
                            Nothing

                acc' = liftA2 countFn (fmap (fmap fst) acc) (fmap (fmap snd) countEnable)
            in
                (acc, delayed (singleton (Nothing)) acc')


maxValues = 15 :> 15 :> 6 :> Nil


data Input = Input
    { countEnable :: Bool
    }


data State = State
    { controlInputs :: Vec 3 (Unsigned 4)
    , done :: Bool
    } deriving Show


initialState :: State
initialState = State
    { controlInputs = 0 :> 0 :> 0 :> Nil
    , done = False
    }


data Output = Output
    { ax0 :: Unsigned 4
    , ax1 :: Unsigned 4
    , ax2 :: Unsigned 4
    , done_ :: Bool
    }



output :: State -> Input -> Output
output State {controlInputs = controlInputs, done = done} input =
    Output
        { ax0 = controlInputs !! 0
        , ax1 = controlInputs !! 1
        , ax2 = controlInputs !! 2
        , done_ = done
        }


nextState :: State -> Input -> State
nextState state input =
    if countEnable input then
        let
            accFn acc val max =
                acc && val == max

            mapFn acc val max =
                if acc then
                    if val == max then 0 else val + 1
                else
                    val

            (_, newInputs) = mapAccumL
                (\acc (val, max) -> (accFn acc val max, mapFn acc val max))
                True
                $ zip (controlInputs state) maxValues

            doneNow = foldl (&&) True
                $ map (\(a,b) -> a == b)
                $ zip newInputs maxValues
        in
            state {controlInputs = newInputs, done = doneNow}
    else
        state


cascadedIteratorT :: State -> Input -> (State, Output)
cascadedIteratorT state input =
    ( nextState state input
    , output state input
    )


-- maskedCounter :: HiddenClockReset domain gated synchronous =>
--     Signal domain a -> Signal domain a
-- spi :: Signal System (Bit, Bit) -> Signal System a
cascadedIterator =
    mealy cascadedIteratorT initialState
    -- delayed clk rst (0 :> 0 :> 0 :> Nil)


{-# ANN topEntity
  (Synthesize
    { t_name   = "CascadedIterator"
    , t_inputs = [ PortName "clk"
                 , PortName "rst"
                 , PortName "count_enable"
                 ]
    , t_output = PortProduct "" 
        [ PortName "ax0"
        , PortName "ax1"
        , PortName "ax2"
        , PortName "done"
        ]
    }) #-}
topEntity
  :: Clock System Source
  -> Reset System Synchronous
  -> Signal System Input
  -> Signal System Output
topEntity = exposeClockReset cascadedIterator



singleValueIteratorT :: (HiddenClockReset domain gated synchronous) =>
    Signal domain (Maybe ((), Bool)) -> Signal domain (Maybe (Unsigned 5, Bool))
singleValueIteratorT input =
    toSignal $ singleValueIterator 4 $ fromSignal input


{-# ANN singleValueIteratorEntity
  (Synthesize
    { t_name   = "SingleValueIterator"
    , t_inputs = [ PortName "clk"
                 , PortName "rst"
                 , PortName "count_enable"
                 ]
    , t_output =
        PortName "result"
    }) #-}
singleValueIteratorEntity
  :: Clock System Source
  -> Reset System Synchronous
  -> Signal System (Maybe ((), Bool))
  -> Signal System (Maybe (Unsigned 5, Bool))
singleValueIteratorEntity = exposeClockReset singleValueIteratorT


